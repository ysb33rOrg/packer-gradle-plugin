= Packer Gradle Plugin
:imagesdir: images
:icons: font

A collection of Gradle plugins to deal with the Packer virtual image building too in a very gradlesque way.

If has been tested using https://github.com/ysb33r/gradleTest[GradleTest] for a wide range of compatibility. The test matrix for this release was

.GradleTest version configuration
[source,groovy]
----
include::{includetopdir}/build.gradle[tags=gradleTestVersions,indent=0]
----


.Product version documentation
[cols="4*"]
|===
include::{gendocdir}/display-versions.adoc[]
|===

== Other links

* https://gitlab.com/ysb33rOrg/packer-gradle-plugin[Fork me on GitLab]


////
== Contributors

A special thank you goes to:

* https://github.com/xyz[XYZ] - DESCRIPTION.

If you would like to contribute, please see https://github.com/ysb33r/grolifant/blob/master/HACKING.adoc[HACKING.adoc]
////
